const finder = document.querySelector(".tabs")
const foinder = document.querySelector(".tabs-content")
finder.addEventListener('click', (event) => {
    const li = event.target.closest('li')
    let active = document.querySelector(".active")
    if(active) {
        active.classList.remove('active')
        finderLi(li)
    }
    li.classList.add('active')
})
function finderLi (li){
    for(let i of foinder.children){
        if(i.dataset.tabs === li.dataset.tabs){
            i.classList.add("active")
            li.classList.remove("active")
        }
        else{
            li.classList.add('active');
            i.classList.remove('active');
        }
    }
}